﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StoreDataApi.Models;

namespace StoreDataApi.Repository
{
    public class GoodRepository : Repository<Good, int>, IGoodRepository
    {
        public List<Good> goods;
        public List<Good> Goods { get; set; }

        public List<Good> GetAllGoods(List<string> data)
        {
            //List<Good> goods = new List<Good>();

            foreach (var line in data)
            {
                var rowData = line.Split(';');

                Good good = new Good();

                good.goodId = Convert.ToInt32(rowData[0]);
                good.transactionId = Convert.ToInt32(rowData[1]);
                good.transactionDate = Convert.ToDateTime(rowData[2]);
                good.amount = Convert.ToInt32(rowData[3]);
                good.direction = rowData[4];
                good.comment = rowData[5];
                goods.Add(good);
            }
            return goods;
        }

        public IEnumerable<Good> GetGoodsById(int goodId, List<Good> goods)
        {
            return goods.Where(g => g.goodId == goodId);
        }

        public List<Good> GetGoodsBetweenDates(List<Good> goods, List<DateTime> dates)
        {
            List<Good> transactions = new List<Good>();
            for (int i = 0; i < goods.Count; i++)
            {
                foreach (var date in dates)
                {
                    if (date == goods[i].transactionDate)
                    {
                        transactions.Add(goods[i]);
                    }
                }
            }
            return transactions;
        }

        public void SortByDate(List<Good> goods)
        {
            goods.Sort((x, y) => y.transactionDate.CompareTo(x.transactionDate));
        }
    }
}
