﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StoreDataApi.Models;

namespace StoreDataApi.Repository
{
    public interface IFileManagerRepository: IRepository<FileManager, int>
    {
    }
}
