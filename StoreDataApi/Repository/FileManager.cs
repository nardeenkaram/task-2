﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StoreDataApi.Models;
using System.IO;

namespace StoreDataApi.Repository
{
    public class FileManager: Repository<Good, int>, IFileManagerRepository
    {
        private FileManager() { }
        private static FileManager fileManager = null;
        public static FileManager ManagerFile
        {
            get
            {
                if (fileManager == null)
                {
                    fileManager = new FileManager();
                }
                return fileManager;
            }
        }

        public string[] ProcessFile(string fileLocation)
        {
            //bool isValid = true;
            //List<Good> goods = new List<Good>();
           return File.ReadAllLines(fileLocation);
            
            //foreach (var line in csvLines)
            //{

            //    isValid = true;
            //    var rowData = line.Split(';');

            //    for (int i = 0; i < rowData.Length - 1; i++)
            //    {
            //        if (String.IsNullOrEmpty(rowData[i]))
            //        {
            //            //Console.WriteLine("line: " + newCsvLine);
            //            isValid = false;

            //            Console.WriteLine("false");
            //            //isValid = false;

            //        }
            //    }
            //    if (isValid)
            //    {
            //        Good good = new Good();

            //        good.goodId = Convert.ToInt32(rowData[0]);
            //        good.transactionId = Convert.ToInt32(rowData[1]);
            //        good.transactionDate = Convert.ToDateTime(rowData[2]);
            //        good.amount = Convert.ToInt32(rowData[3]);
            //        good.direction = rowData[4];
            //        good.comment = rowData[5];
            //        goods.Add(good);
            //    }
            //}
            //return goods;
        }


        //private DataTableViewModel SearchByIdAndDate(int id, DateTime startDate, DateTime endDate, List<Good> goods)
        //{
            
        //}
    }
}
