﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StoreDataApi.Models;

namespace StoreDataApi.Repository
{
    public class Repository<T, ID> : IRepository<T, ID> where T : class
    {
        FileManager fileManager = FileManager.ManagerFile;

        public List<string> GetAll(string path)
        {
            string[] csvData = fileManager.ProcessFile(path);
            bool isValid = true;
            List<string> entity = new List<string>();

            foreach (var line in csvData)
            {

                isValid = true;
                var rowData = line.Split(';');

                for (int i = 0; i < rowData.Length - 1; i++)
                {
                    if (String.IsNullOrEmpty(rowData[i]))
                    {
                        //Console.WriteLine("line: " + newCsvLine);
                        isValid = false;

                        Console.WriteLine("false");
                        //isValid = false;

                    }
                }
                if (isValid)
                {
                    entity.Add(line);
                }
            }
            return entity;
        }

        public List<DateTime> GetBetweenDates(DateTime startDate, DateTime endDate)
        {
            var dates = new List<DateTime>();

            for (var dt = startDate; dt <= endDate; dt = dt.AddDays(1))
            {
                dates.Add(dt);
            }
            return dates;
        }

    }
}
