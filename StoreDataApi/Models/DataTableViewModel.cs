﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoreDataApi.Models
{
    public class DataTableViewModel<T> where T : class
    {
        /// <summary>
        /// Good Data , etc
        /// That class is  generic
        /// </summary>
        public IEnumerable<T> data { get; set; }
        /// <summary>
        /// The Total Number of Filtered Records 
        /// </summary>
        public int recordsNumber { get; set; }
        /// <summary>
        /// The Total Amount
        /// </summary>
        public int recordsAmount { get; set; }
        /// <summary>
        /// The Total Remaining Amount
        /// </summary>
        public int remainingAmount { get; set; }

    }
}
