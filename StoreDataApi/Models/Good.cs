﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoreDataApi.Models
{
    public class Good
    {
        public int goodId { get; set; }
        public int transactionId { get; set; }
        public DateTime transactionDate { get; set; }
        public int amount { get; set; }
        public string direction { get; set; }
        public string comment { get; set; }
        public List<Good> goods { get; set; }

        public bool ValidateGood(string goodLine)
        {
            return true;
        }

        public Good getGoodsById(int goodId, List<Good> goods)
        {
            Good searchGoods = goods.Find(x => x.goodId == goodId);

            return searchGoods;
        }

        public List<Good> SortByDate(List<Good> goods)
        {
            goods.Sort((x, y) => DateTime.Compare(x.transactionDate, y.transactionDate));

            return (List<Good>)goods;
        }

        public List<DateTime> GetBetweenDates(DateTime startDate, DateTime endDate)
        {
            var dates = new List<DateTime>();

            for (var dt = startDate; dt <= endDate; dt = dt.AddDays(1))
            {
                dates.Add(dt);
            }
            return dates;

        }

        public List<Good> getGoodsBetweenDates (List<Good> goods, List<DateTime> dates)
        {
            List<Good> transactions = new List<Good>();
            for(int i = 0; i < goods.Count; i++)
            {
                foreach(var date in dates)
                {
                    if(date == goods[i].transactionDate)
                    {
                        transactions.Add(goods[i]);
                    }
                }
            }
            return transactions;
        }
    }
}
